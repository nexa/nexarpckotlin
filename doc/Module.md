# Module NexaRpc

A library to access the Nexa full node's RPC commands.  This is the same set of commands you get using `nexa-cli`.


## Add to your Gradle Project

[Find the latest library version here.](https://gitlab.com/api/v4/projects/38119368/packages)

In your build.grade.kts:

```gradle
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") } // NexaRpc
}

dependencies {
    // typically this library is used in test code so the example shown uses "testImplementation"
    testImplementation("Nexa","NexaRpc","1.1.0")  // Update this version with the latest
}
```

In your multiplatform build.grade.kts:

```gradle
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") } // NexaRpc
}

dependencies {
    // typically this library is used in test code so the example shown uses "testImplementation"
    testImplementation("Nexa","NexaRpc","1.1.0")  // Update this version with the latest
}
```

# Package Nexa.NexaRpc

*This is the main package to access this library.*

*Simple Example*

``` kotlin
println("This test requires that a nexa regtest network node be running on the localhost at port 18332 with username/password regtest/regtest")
val rpc = NexaRpcFactory.create("http://127.0.0.1:18332", user = "regtest", pwd = "regtest")
var blkhashes = rpc.generate(1)
println("Generated block: ${blkhashes[0]}")

val unspent = rpc.listunspent()
println("There are ${unspent.size} UTXOs in the full node wallet.")

val bal = rpc.getbalance()
println("For a total of $bal NEXA.")

val addr = rpc.getnewaddress()
println("Here is a deposit address for this wallet: $addr")
val tx = rpc.sendtoaddress(addr, BigDecimal("1000.23"))
println("Sent 1000.23 coins to myself in this transaction: $tx")
```



