import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration
import org.jetbrains.dokka.gradle.DokkaTask
import java.io.File
import java.io.FileInputStream
import java.net.URL
import java.util.*

val LINUX = System.getProperty("os.name").lowercase().contains("linux")
val MAC = System.getProperty("os.name").lowercase().contains("mac")
val MSWIN = System.getProperty("os.name").lowercase().contains("windows")

plugins {
    kotlin("multiplatform").version("2.0.0")
    id("com.android.library").version("8.2.0")
    kotlin("plugin.serialization").version("2.0.0")
    id("maven-publish")
    id("org.jetbrains.dokka") version "1.9.20" // https://github.com/Kotlin/dokka
}

buildscript {
    dependencies {
        classpath("org.jetbrains.dokka:dokka-base:1.9.20")
    }
}

group = "org.nexa"
version = "1.2.2"
val ktorVersion = "2.3.11" // https://ktor.io/changelog/
val coroutinesVersion = "1.8.1"  // https://github.com/Kotlin/kotlinx.coroutines
val bigNumVersion = "0.3.9" // https://github.com/ionspin/kotlin-multiplatform-bignum/releases/tag/0.3.9
val kser_version = "1.6.3"  // https://github.com/Kotlin/kotlinx.serialization

val deployTokenKey: String by project


val prop = Properties().apply {
    try
    {
        load(FileInputStream(File(rootProject.rootDir, "local.properties")))
    }
    catch(e:Exception)
    {

    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "17"
}

tasks.dokkaHtml {
    outputDirectory.set(rootDir.resolve("public"))
}

tasks.withType<DokkaTask>().configureEach {

    pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
        customAssets = listOf(file("doc/nexa-64x64.png"), file("doc/logo-icon.svg"))
        customStyleSheets = listOf(file("doc/logo-styles.css"))
        footerMessage = "(c) 2023 Bitcoin Unlimited"
    }

    dokkaSourceSets {
        forEach {
            it.run {
                // named("kotlin") {
                //moduleName.set("Nexa Script Machine")

                displayName.set("Nexa Remote Procedure Call Client Library")
                includes.from("doc/Module.md")
                sourceLink {
                    localDirectory.set(file("src/main/kotlin"))
                    remoteUrl.set(URL("https://gitlab.com/nexa/nexarpckotlin/-/tree/main/"))
                    remoteLineSuffix.set("#L")
                }
                includeNonPublic.set(false)
            }
        }
    }

    dokkaSourceSets.configureEach {

        perPackageOption {
            matchingRegex.set("bitcoinunlimited.*")
            suppress.set(true)
            }
        }
}


kotlin {
    targetHierarchy.default()
    jvm {
        compilations.all {
            kotlinOptions {
                jvmTarget = "17"
            }
        }
    }

    /*
    androidNativeArm32 {
    }
    androidNativeArm64 {
    }
    androidNativeX86 {
    }
     */

    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = "17"
            }
        }
        // publishLibraryVariants("release","debug")
    }

    macosX64 {
    }
    macosArm64 {
    }
    iosArm64 {}
    iosX64 {}
    iosSimulatorArm64 {}
    // MS windows
    mingwX64 {
    }
    linuxX64 {
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
                implementation(kotlin("stdlib-common"))

                // for bigintegers
                implementation("com.ionspin.kotlin:bignum:$bigNumVersion")
                implementation("com.ionspin.kotlin:bignum-serialization-kotlinx:$bigNumVersion")


                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kser_version")
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-serialization:$ktorVersion")
                implementation("io.ktor:ktor-utils:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                // implementation("io.ktor:ktor-network:$ktorVersion")
                // implementation("io.ktor:ktor-network-tls:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                //implementation("org.slf4j","slf4j-api","2.0.7")
                // implementation("ch.qos.logback","logback-classic","1.4.7")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }


        val mingwX64Main by getting {
            dependencies {
                // implementation("io.ktor:ktor-client-cio:$ktorVersion")
                implementation("io.ktor:ktor-client-winhttp:$ktorVersion")
            }
        }

        val linuxX64Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        val androidMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        /*
        val androidNativeX86Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }*/


        val macosX64Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        val macosArm64Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        val iosX64Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        val iosArm64Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }
        /*
        val iosArm32Main by getting {
            dependencies {
                implementation("io.ktor:ktor-client-cio:$ktorVersion")
            }
        }

         */

        val androidInstrumentedTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("androidx.test:core:1.5.0")
                implementation("androidx.test:core-ktx:1.5.0")
                implementation("androidx.test:runner:1.5.2")
                implementation("androidx.test.ext:junit:1.1.5")
                implementation("androidx.test.ext:junit-ktx:1.1.5")
                //implementation("androidx.test.espresso:espresso-core:3.5.1")
            }
        }

        all {
            languageSettings {
                optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
            }
        }
    }
}

android {
    namespace = "org.nexa.nexarpc"
    compileSdk = 34
    defaultConfig {
        minSdk = 29
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}


/*
val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets)
}
*/


publishing {
    /*
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(tasks["sourcesJar"])
        }
    }
     */
    repositories {
        maven {
            // Project ID number is shown just below the project name in the project's home screen
            url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = prop.getProperty("NexaRpcKotlinDeployTokenValue")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

if (MAC)
{
    tasks.withType<PublishToMavenRepository>().configureEach {
        onlyIf {
            it.name.contains("Ios") || it.name.contains("Macos")
        }
    }
}
