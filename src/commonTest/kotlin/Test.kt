import org.nexa.nexarpc.*
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import io.ktor.utils.io.core.toByteArray
import kotlin.test.*

val FULL_NODE_IP = "127.0.0.1"

class NexaRpcTest
{
    @Test
    fun testLaunches()
    {
        println("Test launched")
        check(true)
    }

    // Removed so the automated tests only need regtest to be running @Test
    fun basicTestnet()
    {
        println("This test requires that a nexa regtest network node be running on the localhost at port 7229, with username and password 'testnet'/'testnet'")
        val rpc = NexaRpcFactory.create("http://$FULL_NODE_IP:7229", username = "testnet", password = "testnet")
        val tip = rpc.getbestblockhash()
        println("testnet tip $tip")
    }

    @Test
    fun basicReg()
    {
        println("This test requires that a nexa regtest network node be running on the localhost at port 18332, with username and password 'regtest'/'regtest'")
        val rpc = NexaRpcFactory.create("http://$FULL_NODE_IP:18332", username = "regtest", password = "regtest")

        // clear anything already there
        var blkhashes = rpc.generate(1)
        check(blkhashes.size == 1)
        println("Generated block: ${blkhashes[0]}")
        val tip = rpc.getbestblockhash()
        check(blkhashes[0] == tip)

        val unspent = rpc.listunspent()
        check (unspent.size > 0)
        println("There are ${unspent.size} UTXOs in the full node wallet.")

        val bal = rpc.getbalance()
        check (bal > BigDecimal.ZERO)
        println("For a total of $bal NEXA.")

        val addr = rpc.getnewaddress()
        println("Here is a deposit address for this wallet: $addr")
        val addr2 = rpc.getnewaddress("p2pkh")
        println("Here is an old-style deposit address for this wallet: $addr2")
        check(addr.length > addr2.length) // just a property of the 2 address formats that helps confirm that the right ones were created
        val tx = rpc.sendtoaddress(addr, BigDecimal.parseString("1000.23"))
        println("Sent 1000.23 coins to myself in this transaction: $tx")

        val txpool = rpc.getrawtxpool()
        check(txpool.size == 1)

        val peers = rpc.getpeerinfo()
        assertTrue { peers.size < 100 }  // Probably true anyway


        val txpoolinfo = rpc.gettxpoolinfo()
        assertEquals(txpoolinfo.size, 1)
        assertTrue { txpoolinfo.bytes > 100}

        val rawTx = rpc.getrawtransaction(txpool[0].toHex())
        assertTrue { rawTx.size > 100 }

        val detTx = rpc.gettransactiondetails(txpool[0])
        println(detTx)
        assertTrue { detTx.in_txpool }
        assertTrue { detTx.vin.size > 0 }
        assertTrue { detTx.vout.size == 2 }  // send and change

        val utxoInfo = rpc.getutxo(detTx.vout[0].outpoint)
        println(utxoInfo)
        assertTrue { utxoInfo.confirmations == 0 }
        if (utxoInfo.outpoint != null) assertTrue(utxoInfo.outpoint == detTx.vout[0].outpoint)

        val txi = rpc.gettransaction(txpool[0].toHex())
        println(txi.toString())
        assertEquals(txi.confirmations,0L)
        assertEquals(txi.generated, false)

        rpc.abandontransaction(txpool[0])

        val txpool2 = rpc.getrawtxpool()
        assertEquals(txpool2.size, 0)


        val height = rpc.getblockcount()
        blkhashes = rpc.generate(1)
        assertEquals(blkhashes.size, 1)
        val height2 = rpc.getblockcount()
        assertEquals(height+1, height2)
        rpc.invalidateblock(blkhashes[0])
        val height3 = rpc.getblockcount()
        assertEquals(height, height3)

        val blk = rpc.getblock(0)
        assertEquals(blk.height, 0)
        assertEquals(blk.hash, HashId("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371"))

        rpc.generate(1)
        val tx2id = rpc.sendtoaddress(addr, BigDecimal.parseString("1000.23"))
        val rawTx2 = rpc.getrawtransaction(tx2id)
        rpc.evicttransaction(tx2id)

        val tx2id2 = rpc.enqueuerawtransaction(rawTx2)
        check(tx2id == tx2id2)
        rpc.evicttransaction(tx2id)
        val tx2id3 = rpc.sendrawtransaction(rawTx2)
        check(tx2id == tx2id3)
    }

    @Test
    fun capd()
    {
        println("This test requires that a nexa regtest network node be running on the localhost at port 18332, with username and password 'regtest'/'regtest'")
        val rpc = NexaRpcFactory.create("http://$FULL_NODE_IP:18332", username = "regtest", password = "regtest")

        val aMsg = rpc.capdSend("a test ".toByteArray())
        check(!(aMsg.hash contentEquals ByteArray(20,{0})))

        val msgList = rpc.capdList()
        println(msgList)
        check(aMsg in msgList)

        val info = rpc.capdInfo()
        println(info.size)
        check(info.count > 0)

        val msgDetails = rpc.capdGet(aMsg)
        check(HashId(msgDetails.hash) == aMsg)

        rpc.capdClear()
        val info2 = rpc.capdInfo()
        check(info2.count == 0L)
    }

    @Test fun group()
    {
        println("This test requires that a nexa regtest network node be running on the localhost at port 18332, with username and password 'regtest'/'regtest'")
        val rpc = NexaRpcFactory.create("http://$FULL_NODE_IP:18332", username = "regtest", password = "regtest")

        val addr = rpc.getnewaddress()
        // just some fake hash
        val (grpId, createTx) = rpc.tokenNew(addr, "TST", "Test token", "http://localhost/TSTdesc", "d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371", 2)

        val mintTx = rpc.tokenMint(grpId, addr, 100000)
        val meltTx = rpc.tokenMelt(grpId, 10000)
        rpc.generate(1)
        val (leftOver, leftOverDec) = rpc.tokenMintage(grpId)
        check(leftOver == 100000L-10000L)

        val addr2 = rpc.getnewaddress()
        val authTx = rpc.tokenAuthorityCreate(grpId, addr2, listOf("MINT", "MELT"))
        check(authTx.toHex().length > 0)
        rpc.generate(1)
    }

    @Test fun sign()
    {
        println("This test requires that a nexa regtest network node be running on the localhost at port 18332, with username and password 'regtest'/'regtest'")
        val rpc = NexaRpcFactory.create("http://$FULL_NODE_IP:18332", username = "regtest", password = "regtest")

        val addr = rpc.getnewaddress()
        val addr2 = rpc.getnewaddress()

        val msg = """{ "test message": "is also json"}"""
        val sig = rpc.signMessage(addr, msg)
        check(sig.length > 0)
        val ok = rpc.verifyMessage(addr, sig, msg)
        check(ok)
        var nok = rpc.verifyMessage(addr, sig, "bad msg")
        check(!nok)
        nok = rpc.verifyMessage(addr2, sig, msg)
        check(!nok)

        val sig2 = rpc.signData(addr, "string", msg)
        check(sig2.length > 0)
        check(sig2.fromHex().size == 64)

        val sig3 = rpc.signDataVerbose(addr, "string", msg)
        println(sig3)
    }
}
