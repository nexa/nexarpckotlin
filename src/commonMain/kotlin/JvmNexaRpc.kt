package org.nexa.nexarpc

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.*
import kotlinx.serialization.json.*
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import io.ktor.util.encodeBase64
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.encodeToString
import kotlinx.serialization.encoding.Encoder
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi


//import java.util.logging.Logger
//private val LogIt = Logger.getLogger("Nexa.NexaRpc")


/** Factory to create an RPC connection */
object NexaRpcFactory
{
    //actual
    /** Create the RPC connection
     * @param url String URL to the target full node.  By default http://127.0.0.1:18332/ (regtest) is used
     * @param user RPC authentication username (defined in your full node config file typically located at .nexa/nexa.conf).  Defaults to "regtest"
     * @param pwd RPC authentication password (defined in your full node config file typically located at .nexa/nexa.conf).  Defaults to "regtest"
     */
    fun create(url:String="http://127.0.0.1:18332/", username:String ="regtest", password:String = "regtest"):NexaRpc = JvmNexaRpc(url, username, password)
}

/** Use the [NexaRpcFactory] to create RPC instances instead of directly creating this class */
open class JvmNexaRpc(override val url:String="http://127.0.0.1:18332/", override val user:String ="regtest", override val pwd:String = "regtest"): NexaRpc
{
    var reqCount = 0

    @OptIn(ExperimentalEncodingApi::class)
    val authString = "Basic " + (user + ":" + pwd).encodeBase64()
    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }

    fun formatJsonRpcRequest(method: String, params: List<String>? = null): String
    {
        @Serializable
        data class JsonMsg(
            val method: String,
            val params: List<String>,
            val id: Int
        )
        val jm = JsonMsg(method, params ?: listOf(), reqCount)
        reqCount += 1
        return Json.encodeToString(jm)

        /* Do it manually (doesn't work for strings that are also json)
        val s = StringBuilder()
        val pString = params?.map({ "\"" + it + "\"" })?.joinToString(separator = ",") ?: ""
        s.append("""{ "method" :"${method}", """)
        s.append(""" "params" : [ ${pString} ], """)
        s.append(""" "id" : ${reqCount} }""")
        reqCount += 1
        return s.toString()
         */
    }

    //private val DB = Dispatchers.IO.limitedParallelism(2)
    suspend fun _calls(rpcName: String, params: List<String>? = null): String
    {
        val client = HttpClient()
        try
        {
            val response: HttpResponse = client.post(url)
            {
                headers {
                    append(HttpHeaders.Connection, "close")
                    append(HttpHeaders.Authorization, authString)
                }
                setBody(formatJsonRpcRequest(rpcName, params))
            }
            if (response.status == HttpStatusCode.Unauthorized)
            {
                throw NexaRpcException("Unauthorized (bad rpc username/password)", response.status.value.toLong())
            }
            //println(response.status)
            val respBody: String = response.body()
            // println(respBody)
            return respBody
        }
        catch (e: Exception)
        {
            println(e.toString())
            throw (e)
        }
        finally
        {
            client.close()
        }
    }

    suspend fun _callje(rpcName: String, params: List<String>? = null): JsonElement = json.parseToJsonElement(_calls(rpcName, params))

    /** coroutine version of [listunspent] */
    suspend fun _listunspent(): List<NexaRpc.Unspent>
    {
        val resp = _calls("listunspent")
        val ret = json.decodeFromString(NexaRpc.ListUnspentReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [generate] */
    suspend fun _generate(qty: Int): List<HashId>
    {
        val resp = _calls("generate", listOf(qty.toString()))
        val ret = json.decodeFromString(NexaRpc.HexHashListReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.map() { HashId(it) }
    }

    /** coroutine version of [getrawtxpool] */
    suspend fun _getrawtxpool(): List<HashId>
    {
        val resp = _calls("getrawtxpool")
        val ret = json.decodeFromString(NexaRpc.HexHashListReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.map() { HashId(it) }
    }

    /** coroutine version of [sendtoaddress] */
    suspend fun _sendtoaddress(addr: String, amt: BigDecimal): HashId
    {
        val resp = _calls("sendtoaddress", listOf(addr, amt.toString()))
        val decode = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (decode.result == null)
            throw NexaRpcException(decode.error?.message ?: "Unspecified Error", decode.error?.code ?: 0)
        return HashId(decode.result)
    }

    /** coroutine version of [getbalance] */
    suspend fun _getbalance(): BigDecimal
    {
        val resp = _calls("getbalance")
        val ret = json.decodeFromString(NexaRpc.GetBalanceReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return BigDecimal.fromDouble(ret.result)
    }

    /** coroutine version of [getblockcount] */
    suspend fun _getblockcount(): Long
    {
        val resp = _calls("getblockcount")
        val ret = json.decodeFromString(NexaRpc.LongReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [getbestblockhash] */
    suspend fun _getbestblockhash(): HashId
    {
        val resp = _calls("getbestblockhash")
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null || ret.result.isBlank())
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }

    /** coroutine version of [evicttransaction] */
    suspend fun _evicttransaction(txid: String): Long
    {
        val resp = _calls("evicttransaction", listOf(txid))
        val ret = json.decodeFromString(NexaRpc.LongReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [invalidateblock] */
    suspend fun _invalidateblock(id: String)
    {
        val resp = _calls("invalidateblock", listOf(id))
        val ret = json.decodeFromString(NexaRpc.NothingReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error.message, ret.error.code)
    }

    /** coroutine version of [abandontransaction] */
    suspend fun _abandontransaction(id: String)
    {
        val resp = _calls("abandontransaction", listOf(id))
        val ret = json.decodeFromString(NexaRpc.NothingReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error.message, ret.error.code)
    }

    suspend fun _capdList(): List<HashId>
    {
        val resp = _calls("capd", listOf("list"))
        val ret = json.decodeFromString(NexaRpc.HexHashListReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.map() { HashId(it) }
    }

    /** Clear all capd messages */
    suspend fun _capdClear()
    {
        val resp = _calls("capd", listOf("clear"))
        val ret = json.decodeFromString(NexaRpc.NothingReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error.message, ret.error.code)
    }

    /** get a particular CAPD message */
    suspend fun _capdGet(msg: HashId):NexaRpc.CapdMsg
    {
        val resp = _calls("capd", listOf("get", msg.toHex()))
        val ret = json.decodeFromString(NexaRpc.GetCapdMsgReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** Create a new CAPD message */
    suspend fun _capdSend(msg: ByteArray):HashId
    {
        val resp = _calls("capd", listOf("send", msg.toHex()))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error.message, ret.error.code)
        return HashId(ret.result!!)
    }

    /** Get info about the capd subsystem */
    suspend fun _capdInfo(): NexaRpc.CapdInfo
    {
        val resp = _calls("capd", listOf("info"))
        val ret = json.decodeFromString(NexaRpc.GetCapdInfoReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [getwalletinfo] */
    suspend fun _getwalletinfo(): NexaRpc.WalletInfo
    {
        val resp = _calls("getwalletinfo")
        val ret = json.decodeFromString(NexaRpc.GetWalletInfoReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [getblock] */
    suspend fun _getblock(which:String): NexaRpc.BlockInfo
    {
        val resp = _calls("getblock", listOf(which))
        val ret = json.decodeFromString(NexaRpc.GetBlockReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [getpeerinfo] */
    suspend fun _getpeerinfo(): List<NexaRpc.PeerInfo>
    {
        val resp = _calls("getpeerinfo")
        //println(" getpeerinfo: " + resp)
        val ret = json.decodeFromString(NexaRpc.GetPeerInfoReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error.message, ret.error.code)
        return ret.result
    }

    /** coroutine version of [getnewaddress] */
    suspend fun _getnewaddress(addrType:String?): String
    {
        val resp = _calls("getnewaddress", if (addrType != null) listOf(addrType) else null)
        val ret = json.decodeFromString(NexaRpc.GetNewAddressReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [getrawtransaction] */
    suspend fun _gettransactiondetails(txhash:String): NexaRpc.TransactionDetails
    {
        val resp = _calls("getrawtransaction", listOf(txhash, "true"))
        val ret = json.decodeFromString(NexaRpc.GetTransactionDetailsReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _getrawtransaction(txhash:String): ByteArray
    {
        val resp = _calls("getrawtransaction", listOf(txhash))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.fromHex()
    }

    suspend fun _sendrawtransaction(txHex:String): HashId
    {
        val resp = _calls("sendrawtransaction", listOf(txHex))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }

    suspend fun _enqueuerawtransaction(txHex:String): HashId
    {
        val resp = _calls("enqueuerawtransaction", listOf(txHex))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }



    /** coroutine version of [gettransaction] */
    suspend fun _gettransaction(txhash: String): NexaRpc.TransactionInfo
    {
        val resp = _calls("gettransaction", listOf(txhash))
        val ret = json.decodeFromString(NexaRpc.GetTransactionReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** coroutine version of [gettransaction] */
    suspend fun _getutxo(txhash: String): NexaRpc.Txout
    {
        val resp = _calls("gettxout", listOf(txhash))
        val ret = json.decodeFromString(NexaRpc.GetTxoutReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }


    /** coroutine version of [gettxpoolinfo] */
    suspend fun _gettxpoolinfo(): NexaRpc.TxPoolInfo
    {
        val resp = _calls("gettxpoolinfo")
        val ret = json.decodeFromString(NexaRpc.TxPoolInfoReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _tokenNew(address: String?, tokenTicker: String?, tokenName: String?, descUrl: String?, descHash: String?, decimals: Int?):Pair<String, HashId>
    {
        val resp = _calls("token", listOfNotNull("new", address, tokenTicker, tokenName, descUrl, descHash, decimals?.toString()))
        val ret = json.decodeFromString(NexaRpc.TokenNewReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return Pair(ret.result.groupIdentifier, HashId(ret.result.transaction))
    }

    suspend fun _tokenMint(groupId: String, address: String, quantity: Int):HashId
    {
       val resp = _calls("token", listOf("mint", groupId, address, quantity.toString()))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }

    suspend fun _tokenMelt(groupId: String, quantity: Int):HashId
    {
        val resp = _calls("token", listOf("melt", groupId, quantity.toString()))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }

    suspend fun _tokenBalance(groupId: String, address: String?): Pair<Long, String>
    {
        val resp = _calls("token", listOf("melt", groupId))
        val ret = json.decodeFromString(NexaRpc.TokenBalanceReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return Pair(ret.result.balance_satoshis, ret.result.decimals)
    }

    suspend fun _tokenMintage(groupId: String): Pair<Long, String>
    {
        val resp = _calls("token", listOf("mintage", groupId))
        val ret = json.decodeFromString(NexaRpc.TokenMintageReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return Pair(ret.result.mintage_satoshis, ret.result.decimals)
    }

    suspend fun _tokenSend(groupId: String, address: String, quantity: Int): HashId
    {
        val resp = _calls("token", listOf("send", groupId, address, quantity.toString()))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }

    suspend fun _tokenAuthorityCreate(groupId: String, address: String, authFlags: List<String>): HashId
    {
        val resp = _calls("token", listOf("authority", "create", groupId, address) + authFlags)
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return HashId(ret.result)
    }

    suspend fun _signMessage(address: String, message: String): String
    {
        val resp = _calls("signmessage", listOf(address, message))
        val ret = json.decodeFromString(NexaRpc.StringReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _verifyMessage(address:String, signature:String, message:String):Boolean
    {
        val resp = _calls("verifymessage", listOf(address, signature, message))
        val ret = try
        {
            val ret = json.decodeFromString(NexaRpc.BooleanReply.serializer(), resp)
            if (ret.result == null)
                throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
            ret.result
        }
        catch(e: SerializationException)
        {
            val ret = json.decodeFromString(NexaRpc.StringReply.serializer(), resp)
            if (ret.result == null)
                throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
            if (ret.result == "Verify Message Failed") false
            else throw NexaRpcException("Incorrect verify message string reply: ${ret.result}", -1)
        }
        return ret
    }

    suspend fun _signData(address: String, format: String, message: String): String
    {
        val resp = _calls("signdata", listOf(address, format, message))
        val ret = json.decodeFromString(NexaRpc.StringReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _signDataVerbose(address: String, format: String, message: String): NexaRpc.SignData
    {
        val resp = _calls("signdata", listOf(address, format, message, "verbose"))
        val ret = json.decodeFromString(NexaRpc.SignDataReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    /** Alias for [getpeerinfo] */
    val peerInfo: List<NexaRpc.PeerInfo>
        get() = getpeerinfo()


    @Suppress("Dokka")
    fun _bch_getmempoolinfo():NexaRpc.BchMemPoolInfo
    {
        var result: NexaRpc.BchMemPoolInfo? = null
        runBlocking {
            val resp = _calls("getmempoolinfo")
            val ret:NexaRpc.BchMemPoolInfoReply = json.decodeFromString(NexaRpc.BchMemPoolInfoReply.serializer(), resp)
            if (ret.result == null)
                throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
            result = ret.result
        }
        return result!!
    }

    override fun getpeerinfo() = runBlocking { _getpeerinfo() }

    override fun listunspent() = runBlocking { _listunspent() }

    override fun generate(qty: Int) = runBlocking { _generate(qty) }
    override fun getrawtxpool() = runBlocking { _getrawtxpool() }
    override fun sendtoaddress(addr: String, amt: BigDecimal) = runBlocking { _sendtoaddress(addr, amt) }
    override fun getbalance() = runBlocking { _getbalance() }
    override fun getwalletinfo() = runBlocking { _getwalletinfo() }
    override fun calls(rpcName: String, params:List<String>?): String = runBlocking { _calls(rpcName, params) }
    override fun callje(rpcName: String, params:List<String>?): JsonElement = runBlocking { _callje(rpcName, params) }
    override fun getnewaddress(addrType:String?) = runBlocking { _getnewaddress(addrType)}
    override fun gettxpoolinfo() = runBlocking { _gettxpoolinfo() }

    override fun getrawtransaction(hash:String): ByteArray = runBlocking { _getrawtransaction(hash)}
    override fun getrawtransaction(hash:HashId): ByteArray = getrawtransaction(hash.toHex())

    override fun gettransaction(hash:String) = runBlocking { _gettransaction(hash)}

    override fun sendrawtransaction(txHex:String): HashId = runBlocking { _sendrawtransaction(txHex)}
    override fun sendrawtransaction(tx:ByteArray): HashId = sendrawtransaction(tx.toHex())

    override fun enqueuerawtransaction(txHex:String): HashId = runBlocking { _enqueuerawtransaction(txHex)}
    override fun enqueuerawtransaction(tx:ByteArray): HashId = enqueuerawtransaction(tx.toHex())

    override fun gettransactiondetails(hash:String) = runBlocking { _gettransactiondetails(hash)}
    override fun getutxo(hash:String) = runBlocking { _getutxo(hash) }

    override fun getblock(hash:HashId): NexaRpc.BlockInfo = runBlocking {_getblock(hash.toString())}
    override fun getblock(height:Long): NexaRpc.BlockInfo = runBlocking {_getblock(height.toString())}

    override fun getblockcount(): Long = runBlocking { _getblockcount() }

    override fun getbestblockhash(): HashId = runBlocking { _getbestblockhash() }

    override fun evicttransaction(hash: String)  = runBlocking { _evicttransaction(hash) }

    override fun invalidateblock(hash: String)  = runBlocking { _invalidateblock(hash) }

    override fun abandontransaction(hash: String)  = runBlocking { _abandontransaction(hash) }

    override fun capdList(): List<HashId> = runBlocking { _capdList() }
    /** Clear all capd messages */
    override fun capdClear() = runBlocking { _capdClear() }

    /** get a particular CAPD message */
    override fun capdGet(msg: HashId): NexaRpc.CapdMsg = runBlocking { _capdGet(msg) }

    /** Create a new CAPD message */
    override fun capdSend(msg: ByteArray) = runBlocking { _capdSend(msg) }

    /** Get info about the capd subsystem */
    override fun capdInfo(): NexaRpc.CapdInfo = runBlocking { _capdInfo() }
    override fun tokenNew(
        address: String?,
        tokenTicker: String?,
        tokenName: String?,
        descUrl: String?,
        descHash: String?,
        decimals: Int?
    ):Pair<String, HashId> = runBlocking { _tokenNew(address, tokenTicker, tokenName, descUrl, descHash, decimals) }

    override fun tokenMint(groupId: String, address: String, quantity: Int):HashId = runBlocking { _tokenMint(groupId, address, quantity) }

    override fun tokenMelt(groupId: String, quantity: Int):HashId = runBlocking { _tokenMelt(groupId, quantity) }

    override fun tokenBalance(groupId: String, address: String?): Pair<Long, String> = runBlocking { _tokenBalance(groupId, address) }

    override fun tokenMintage(groupId: String): Pair<Long, String> = runBlocking { _tokenMintage(groupId) }

    override fun tokenSend(groupId: String, address: String, quantity: Int): HashId = runBlocking { _tokenSend(groupId, address, quantity) }

    override fun tokenAuthorityCreate(groupId: String, address: String, authFlags: List<String>): HashId = runBlocking { _tokenAuthorityCreate(groupId, address, authFlags) }

    override fun signMessage(address: String, message: String): String = runBlocking { _signMessage(address, message)}
    override fun verifyMessage(address:String, signature:String, message:String):Boolean = runBlocking { _verifyMessage(address, signature, message) }
    override fun signData(address: String, format: String, message: String): String = runBlocking { _signData(address, format, message) }
    override fun signDataVerbose(address: String, format: String, message: String): NexaRpc.SignData = runBlocking { _signDataVerbose(address, format, message)}

    override fun bch_getmempoolinfo() = runBlocking { _bch_getmempoolinfo() }



}
