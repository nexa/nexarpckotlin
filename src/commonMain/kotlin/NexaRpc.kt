package org.nexa.nexarpc

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonElement
import com.ionspin.kotlin.bignum.decimal.BigDecimal

/** All exceptions thrown from this library derive from this class */
open class NexaRpcException(msg:String, val code: Long):Exception(msg)

private val HEX_CHARS = "0123456789abcdef"
// Convert a hex string to a ByteArray
fun String.fromHex(): ByteArray
{

    val result = ByteArray(length / 2)

    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i]);
        val secondIndex = HEX_CHARS.indexOf(this[i + 1]);

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

fun Byte.toUint():Int = toInt() and 0xFF

fun ByteArray.toHex(): String
{
    val ret = StringBuilder()
    for (b in this)
    {
        ret.append(HEX_CHARS[b.toUint() shr 4])
        ret.append(HEX_CHARS[b.toUint() and 0xf])
    }
    return ret.toString()
}

/** A hash identifier (as a byte array) */
@Serializable(with = HashIdAsStringSerializer::class)
class HashId(val hash: ByteArray = ByteArray(32, { _ -> 0 }))
{
    constructor(hex: String) : this()
    {
        val hsh = hex.fromHex()
        hsh.reverse()
        hsh.copyInto(hash)
    }

    /** Convert to the bitcoin standard hex representation */
    fun toHex(): String
    {
        val cpy = hash.copyOf()
        cpy.reverse()
        return cpy.toHex()
    }

    /** The default display will be bitcoin standard hex representation (reversed hex) */
    override fun toString(): String = this.toHex()

    override fun equals(other: Any?): Boolean
    {
        if (other is HashId) return hash contentEquals other.hash
        return false
    }
}

/** @suppress */
private object HashIdAsStringSerializer: KSerializer<HashId>
{
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("HashId", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: HashId)
    {
        val string = value.toHex()
        encoder.encodeString(string)
    }

    override fun deserialize(decoder: Decoder): HashId
    {
        val string = decoder.decodeString()
        return HashId(string)
    }
}

/** Functions to access the Nexa full node */
interface NexaRpc
{
    /** @suppress */
    @Serializable
    data class RpcError(val code:Long, val message: String)

    @Serializable
    // Do not use the amount field because it is not a decimal fraction.  Use the satoshis field.
    data class Unspent(val outpoint: String, val txid: String, val txidem: String, val vout: Long, val address:String, val scriptPubKey: String, val scriptType: String, val satoshi: Long, val amount: Double, val confirmations: Long, val spendable: Boolean)

    /** @suppress */
    @Serializable
    data class ListUnspentReply(val result: List<Unspent>?, val error: RpcError?)

    /** @suppress */
    @Serializable
    data class GetBalanceReply(val result: Double?, val error: RpcError?)  // TODO change to BigDecimal when supported by Kotlin

    @Serializable
    data class WalletInfo(val walletversion: Long, val syncblock: String, val syncheight:Long, val Balance: Double, val unconfirmed_balance: Double, val immature_balance: Double, val txcount: Long, val keypoololdest: Long, val keypoolsize:Long, val paytxfee: Long, val hdmasterkeyid:String)
    /** @suppress */
    @Serializable
    data class GetWalletInfoReply(val result: WalletInfo?, val error: RpcError?)


    @Serializable
    data class CapdInfo(val size: Long, val count: Long, val minPriority: Double, val maxPriority: Double, val relayPriority: Double)
    /** @suppress */
    @Serializable
    data class GetCapdInfoReply(val result: CapdInfo?, val error: RpcError?)

    @Serializable
    data class CapdMsg(val hash: String, val created: Long, val difficultyBits: Long, val priority: Double, val initialPriority: Double,
        val powTarget: String, val nonce: String, val size: Long, val data: String)
    @Serializable
    data class GetCapdMsgReply(val result: CapdMsg?, val error: RpcError?)

    /** @suppress */
    @Serializable
    data class GetNewAddressReply(val result: String?, val error: RpcError?)

    @Serializable
    /** Information about the current transaction pool.  The transaction pool are valid transactions waiting to be confirmed in a block */
    data class TxPoolInfo(
        /** The number of transactions in the pool */
        val size: Long,
        /** The size of the pool if all transactions were serialized */
        val bytes: Long,
        /** Total memory usage for the transaction pool */
        val usage: Long,
        /** Maximum memory usage for the transaction pool */
        val maxtxpool: Long,
        /** Minimum fee for tx to be accepted */
        val txpoolminfee: Double,
        /** Transactions per second accepted into the pool */
        val tps: Double,
        /**  Peak Transactions per second accepted into the pool */
        val peak_tps: Double)

    /** @suppress */
    @Serializable
    data class TxPoolInfoReply (val result: TxPoolInfo?, val error: RpcError?)

    @Serializable
    data class PeerInfo(val id:Long, val addr: String, val addrlocal: String?=null, val services: String, val servicesnames: List<String>, val relaytxes: Boolean, val lastsend: Long, val lastrecv: Long,
                        val bytessent: Long, val bytesrecv:Long, val conntime: Long, val timeoffset: Long, val pingtime:Double, val minping: Double, val version: Long, val subver: String, val inbound: Boolean,
                        val startingheight: Long, val banscore: Long, val synced_headers: Long, val synced_blocks: Long, val inflight: List<String>, val whitelisted: Boolean, val extversion_map: Map<String, String>)
    /** @suppress */
    @Serializable
    data class GetPeerInfoReply(val result: List<PeerInfo>, val error: RpcError?)

    @Serializable data class BlockInfo(val hash: HashId, val confirmations: Long, val height: Long, val size: Long, val feePoolAmt: Long, val merkleroot:HashId, val time: Long, val mediantime: Long,
                                       val nonce: String, val bits: String, val difficulty: Double, val chainwork: String, val utxoCommitment: String, val minerData: String, val ancestorhash: HashId, val nextblockhash: HashId? = null,
                                       val txid: List<HashId>, val txidem: List<HashId>)
    /** @suppress */
    @Serializable
    data class GetBlockReply(val result: BlockInfo?, val error: RpcError?)

    @Serializable
    data class ScriptPubKey(val asm: String, val hex: String, @SerialName("type") val typ: String,
        val scriptHash: String, val argsHash: String, val addresses: List<String>)

    @Serializable
    data class Txout(val outpoint: String? = null, val bestblock: HashId, val confirmations: Int, val value: Double, val scriptPubKey : ScriptPubKey)

    /** @suppress */
    @Serializable
    data class GetTxoutReply(val result: Txout?, val error: RpcError?)


    @Serializable
    /** @suppress */
    data class LongReply(val result: Long?, val error: RpcError?)

    /** @suppress */
    @Serializable
    data class NothingReply(val error: RpcError?)

    /** @suppress */
    @Serializable
    data class StringReply(val result: String?, val error: RpcError?)

    /** @suppress */
    @Serializable
    data class BooleanReply(val result: Boolean?, val error: RpcError?)

    /** @suppress */
    @Serializable
    data class HexHashReply(val result: String?, val error: RpcError?)
    /** @suppress */
    @Serializable
    data class HexHashListReply(val result: List<String>?, val error: RpcError?)

    @Serializable data class TxDetail(val account: String, val address: String, val category: String, val satoshi: Long, val amount: Double, val vout: Long)
    @Serializable data class TransactionInfo(val satoshi: Long, val amount: Double, val confirmations: Long, val generated: Boolean = false, val blockhash: HashId? = null, val blockindex: Long?=null, val blocktime: Long?=null,
    val txid: HashId, val txidem: HashId, val walletconflicts: List<HashId>, val time: Long, val timereceived: Long, val details: List<TxDetail>, val hex: String)
    /** @suppress */
    @Serializable
    data class GetTransactionReply(val result: TransactionInfo?, val error: RpcError?)

    @Serializable data class AsmHex(val asm: String, val hex: String)
    @Serializable data class Vin(val outpoint: String, val amount: Double, val scriptSig: AsmHex, val sequence: Long)
    @Serializable data class Vout(val value: Double, @SerialName("type") val typ: Int, val n: Int, val scriptPubKey: ScriptPubKey, val outpoint: String)
    @Serializable data class TransactionDetails(val in_txpool: Boolean, val in_orphanpool: Boolean, val txid: HashId, val txidem: HashId,
        val size: Long, val version: Long, val locktime: Long, val spends: Double, val sends: Double, val fee: Double,
        val vin: List<Vin>,
        val vout: List<Vout>,
        val blockhash: HashId? = null, val blockindex: Long?=null, val blocktime: Long?=null,
        val time: Long, val hex: String)
    /** @suppress */
    @Serializable
    data class GetTransactionDetailsReply(val result: TransactionDetails?, val error: RpcError?)

    @Serializable
    /** Information about the current transaction pool.  The transaction pool are valid transactions waiting to be confirmed in a block */
    data class TokenBalance(
        /** The balance in the finest unit */
        val balance_satoshis: Long,
        /** The balance in the units the token wants to show (or empty string) */
        val decimals: String)

    /** @suppress */
    @Serializable
    data class TokenBalanceReply (val result: TokenBalance?, val error: RpcError?)

    @Serializable
    /** returned data when a group is created */
    data class TokenNew(
        val groupIdentifier: String,
        val transaction: String)

    /** @suppress */
    @Serializable
    data class TokenNewReply (val result: TokenNew?, val error: RpcError?)

    @Serializable
    /** Information about the current transaction pool.  The transaction pool are valid transactions waiting to be confirmed in a block */
    data class TokenMintage(
        /** The amount minted in the finest unit */
        val mintage_satoshis: Long,
        /** The amount minted in the units the token wants to show (or empty string) */
        val decimals: String)

    /** @suppress */
    @Serializable
    data class TokenMintageReply (val result: TokenMintage?, val error: RpcError?)

    @Serializable
    /** Information about the current transaction pool.  The transaction pool are valid transactions waiting to be confirmed in a block */
    data class SignData(
        /** the hash of the data you provided */
        val msghash: String,
        /** The hex-encoded signature */
        val signature: String,
        /** the hex-encoded hash of the signing pubkey */
        val pubkeyhash: String,
        /** the hex-encoded signing pubkey */
        val pubkey: String,
        )

    /** @suppress */
    @Serializable
    data class SignDataReply (val result: SignData?, val error: RpcError?)

    /// Some BCH/Bitcoin RPCs are also added for ease of interoperability:
    @Serializable
    data class BchMemPoolInfo(val size: Long, val bytes: Long, val usage: Long, val maxmempool: Long, val mempoolminfee: Double, val tps: Double, val peak_tps: Double)
    /** @suppress */
    @Serializable
    data class BchMemPoolInfoReply (val result: BchMemPoolInfo?, val error: RpcError?)

    val url:String?
    val user:String?
    val pwd:String?

    fun callje(rpcName: String, params:List<String>?=null): JsonElement
    fun calls(rpcName: String, params:List<String>?=null): String

    /** list unspent coins in this wallet */
    fun listunspent(): List<Unspent>

    /** Generate (mine) blocks (regtest or testnet only)
     * @param qty Number of blocks to mine
     * @return a list of the mined blocks' hashes */
    fun generate(qty: Int):List<HashId>

    /** Get the hash of every transaction in the transaction pool (waiting to be committed into a block)
     * @return a list of the hashes of all transactions in the pool.
     * */
    fun getrawtxpool():List<HashId>
    /** Get statistics about the transaction pool */
    fun gettxpoolinfo(): TxPoolInfo
    /** Send Nexa to an address */
    fun sendtoaddress(addr: String, amt: BigDecimal):HashId
    /** Get the balance of the full node wallet */
    fun getbalance(): BigDecimal
    /** Return statistics about the full node wallet */
    fun getwalletinfo(): WalletInfo
    /** Get information about the nodes this full node is connected to */
    fun getpeerinfo(): List<PeerInfo>
    /** Get a new deposit address from the full node wallet
     * @return The address in string format */
    fun getnewaddress(addrType:String? = null): String

    /** Get the transaction that is identified by the passed hash
     * @throws NexaRpcException if the transaction is unknown
     * @return The serialized transaction as raw bytes (this can be parsed using (libnexakotlin)[https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin])
     */
    fun getrawtransaction(hash:String): ByteArray
    /** Get the transaction that is identified by the passed hash
     * @throws NexaRpcException if the transaction is unknown
     * @return The serialized transaction as raw bytes
     */
    fun getrawtransaction(hash:HashId): ByteArray

    /** Place the provided hex transaction into the send queue (if valid).  Do not wait for
     * transaction verification.
     * @throws NexaRpcException if the passed string cannot be deserialized into a transaction
     * @return nothing
     */
    fun enqueuerawtransaction(txHex:String): HashId

    /** Place the provided hex transaction into the send queue (if valid).  Do not wait for
     * transaction verification.
     * @throws NexaRpcException if the passed string cannot be deserialized into a transaction
     * @return nothing
     */
    fun enqueuerawtransaction(tx:ByteArray): HashId


    /** Check the provided hex transaction for validity and return if invalid.  Otherwise add to send queue
     * @throws NexaRpcException if the transaction is invalid
     * @return nothing
     */
    fun sendrawtransaction(txHex:String): HashId

    /** Check the provided hex transaction for validity and return if invalid.  Otherwise add to send queue
     * @throws NexaRpcException if the transaction is invalid
     * @return nothing
     */
    fun sendrawtransaction(tx:ByteArray): HashId


    /** Get the decoded transaction that is identified by the passed hash
     * @throws NexaRpcException if the transaction is unknown
     * */
    fun gettransactiondetails(hash:String): TransactionDetails
    /** Get the decoded transaction that is identified by the passed hash
     * @throws NexaRpcException If the transaction is unknown
     * */
    fun gettransactiondetails(hash:HashId) = gettransactiondetails(hash.toHex())


    /** Get the decoded transaction that is identified by the passed hash (if its in your wallet)
     * @throws NexaRpcException if the transaction is unknown
     * */
    fun gettransaction(hash:String): TransactionInfo
    /** Get the decoded transaction that is identified by the passed hash
     * @throws NexaRpcException If the transaction is unknown
     * */
    fun gettransaction(hash:HashId) = gettransaction(hash.toHex())


    /** Get the decoded unspent transaction output (UTXO) that is identified by the passed outpoint hash
     * @throws NexaRpcException If the transaction is unknown
     * */
    fun getutxo(hash: String): Txout

    /** Get the decoded unspent transaction output (UTXO) that is identified by the passed outpoint hash
     * @throws NexaRpcException If the transaction is unknown
     * */
    fun getutxo(hash:HashId) = getutxo(hash.toHex())


    /** Get the decoded block that is identified by the passed hash
     * @throws NexaRpcException If the block does not exist
     */
    fun getblock(hash:HashId): BlockInfo
    /** Get the decoded block that is identified by the passed height
     * @throws NexaRpcException If height exceeds the blockchain
     */
    fun getblock(height:Long): BlockInfo

    /** get the height of the blockchain according to this node
     * @return height Long
     */
    fun getblockcount(): Long

    /** get the hash of the blockchain tip
     * @return hash HashId
     */
    fun getbestblockhash(): HashId


    /** Remove transaction from txpool.  Note that it could be re-added quickly if relayed by another node. */
    fun evicttransaction(hash: HashId) = evicttransaction(hash.toHex())

    /** Remove transaction from txpool.  Note that it could be re-added quickly if relayed by another node. */
    fun evicttransaction(hash: String): Long

    /** Permanently marks a block as invalid, as if it violated a consensus rule. */
    fun invalidateblock(hash: HashId) = invalidateblock(hash.toHex())

    /** Permanently marks a block as invalid, as if it violated a consensus rule. */
    fun invalidateblock(hash: String)

    /** Mark in-wallet transaction as abandoned
     *
     * This will mark this transaction and all its in-wallet descendants as abandoned which will allow
     * for their inputs to be respent.  It can be used to replace "stuck" or evicted transactions.
     * It only works on transactions which are not included in a block.  It removes transactions currently
     * in the txpool.  It has no effect on transactions which are already conflicted or abandoned.
     *
     * @param hash Either the txid or txidem
     */
    fun abandontransaction(hash: HashId) = abandontransaction(hash.toHex())

    /** Mark in-wallet transaction as abandoned
     *
     * This will mark this transaction and all its in-wallet descendants as abandoned which will allow
     * for their inputs to be respent.  It can be used to replace "stuck" or evicted transactions.
     * It only works on transactions which are not included in a block.  It removes transactions currently
     * in the txpool.  It has no effect on transactions which are already conflicted or abandoned.
     *
     * @param hash Either the txid or txidem
     */
    fun abandontransaction(hash: String)

    /** Get list of capd messages */
    fun capdList():List<HashId>

    /** Clear all capd messages */
    fun capdClear()

    /** get a particular CAPD message */
    fun capdGet(msg: HashId):CapdMsg

    /** Create a new CAPD message */
    fun capdSend(msg: ByteArray):HashId

    /** Get info about the capd subsystem */
    fun capdInfo():CapdInfo

    fun tokenNew(address: String?=null, tokenTicker:String?=null, tokenName:String?=null, descUrl:String?=null, descHash:String?=null, decimals:Int?=null):Pair<String, HashId>
    fun tokenMint(groupId: String, address: String, quantity: Int):HashId
    fun tokenMelt(groupId: String, quantity: Int):HashId
    fun tokenBalance(groupId: String,address: String?=null): Pair<Long, String>
    fun tokenMintage(groupId: String): Pair<Long, String>
    fun tokenSend(groupId: String,address: String, quantity: Int): HashId
    fun tokenAuthorityCreate(groupId: String, address: String, authFlags: List<String>): HashId

    fun signMessage(address: String, message: String): String
    fun verifyMessage(address:String, signature:String, message:String):Boolean
    fun signData(address: String, format: String, message: String): String
    fun signDataVerbose(address: String, format: String, message: String): SignData

    /** If connected to a Bitcoin Cash node, this returns the txpool info
    * Some BCH specific functions are implemented for ease of interop testing
     */
    fun bch_getmempoolinfo(): BchMemPoolInfo
}

/*
expect object NexaRpcFactory
{
    fun create(url:String="http://10.0.2.2:18332/", user:String ="regtest", pwd:String = "regtest"): NexaRpc
}
 */


